# Nginx

六种负载均衡方式：（作用也是避免请求过了集中到一台机器）

> 1. 轮询
> 2. weight权重方式
> 3. ip_hash根据ip分配方式，可以让同一个用户访问同一个资源。
> 4. least_conn最少链接方式
> 5. fair响应时间方式，请求向响应时间较短的机器发。
> 6. url_hash依据URL分配方式，根据url请求到同一个服务器，避免缓存资源多次请求。

**配置相关**

1. event标签里面，有**worker_processer**（表示连接处理请求的线程个数，一般和cpu核数保持一致），**worker_connections**（每一个worker最大连接数，一般和ulimit -a中的openfile参数[这个参数表示操作系统最大打开文件数量，最好改大些，我见过netty实战中直接把它改成100万了]除以worker_processer数）。
2. **keepalive_timeout、send timeout **请求和响应的超时时间。
3. **default_type  application/octet-stream;**将一些路径下的例如文件通知到浏览器是文件流。
4. **client_max_body_size 10m;**上传文件大小限制。
5. 转发配置


	server{
		  listen 80;
		  charset utf-8;
		  server_name www.ican.com;
		  access_log logs/host.access.log;
		  location /{
			  proxy_pass http://127.0.0.1:7001;
		  }
	}

以下知识来自：https://www.cnblogs.com/binghe001/p/13273114.html

**限流模块**：

1、limit_req_zone：限制单位时间内的请求数，漏桶算法。

2、limit_req_conn：限制同一时间连接数，即并发限制。

### limit_req_zone参数说明

```bash
Syntax: limit_req zone=name [burst=number] [nodelay];
Default:    —
Context:    http, server, location
limit_req_zone $binary_remote_addr zone=one:10m rate=1r/s;
```

- 第一个参数：$binary_remote_addr 表示通过remote_addr这个标识来做限制，“binary_”的目的是缩写内存占用量，是限制同一客户端ip地址。
- 第二个参数：zone=one:10m表示生成一个大小为10M，名字为one的内存区域，用来存储访问的频次信息。
- 第三个参数：rate=1r/s表示允许相同标识的客户端的访问频次，这里限制的是每秒1次，还可以有比如30r/m的。

```bash
limit_req zone=one burst=5 nodelay;
```

- 第一个参数：zone=one 设置使用哪个配置区域来做限制，与上面limit_req_zone 里的name对应。
- 第二个参数：burst=5，重点说明一下这个配置，burst爆发的意思，这个配置的意思是设置一个大小为5的缓冲区当有大量请求（爆发）过来时，超过了访问频次限制的请求可以先放到这个缓冲区内。
- 第三个参数：nodelay，如果设置，超过访问频次而且缓冲区也满了的时候就会直接返回503，如果没有设置，则所有请求会等待排队。

示例：

### limit_req_zone示例

```bash
http {
    limit_req_zone $binary_remote_addr zone=one:10m rate=1r/s;
    server {
        location /search/ {
            limit_req zone=one burst=5 nodelay;
        }
}
```

### ngx_http_limit_conn_module 参数说明

这个模块用来限制单个IP的请求数。并非所有的连接都被计数。只有在服务器处理了请求并且已经读取了整个请求头时，连接才被计数。

```bash
Syntax: limit_conn zone number;
Default:    —
Context:    http, server, location
limit_conn_zone $binary_remote_addr zone=addr:10m;
 
server {
    location /download/ {
        limit_conn addr 1;
    }
```

一次只允许每个IP地址一个连接。

```bash
limit_conn_zone $binary_remote_addr zone=perip:10m;
limit_conn_zone $server_name zone=perserver:10m;
 
server {
    ...
    limit_conn perip 10;
    limit_conn perserver 100;
}
```

可以配置多个limit_conn指令。例如，以上配置将限制每个客户端IP连接到服务器的数量，同时限制连接到虚拟服务器的总数。