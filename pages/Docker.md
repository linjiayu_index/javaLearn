# Docker

**安装**：参考https://yeasy.gitbook.io/docker_practice/install

**主要命令（以安装nginx为例）：**

 service docker restart  //重启docker

docker search nginx //搜索镜像

docker pull nginx:latest //拉取镜像

docker images  //列举已有镜像

docker run --name nginx-test -p 8080:80 -d nginx  //启动镜像

docker exec -it nginx-test /bin/bash  //进入镜像

docker container stop 102edb0e4a22  //关闭指定ID容器

docker container kill 102edb0e4a22  //杀死指定ID容器

docker container rm 102edb0e4a22  //删除ID容器



# 安装 Docker 的可视化管理系统
docker volume create portainer_data
docker run -d -p 9000:9000 --name portainer --restart always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer
Docker 可视化管理系统运行在了服务器的9000 端口上